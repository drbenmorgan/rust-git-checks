// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug, Default, Clone, Copy)]
/// Rejects the addition of symlinks to a repository.
pub struct RejectSymlinks;

impl RejectSymlinks {
    /// Create a new check which rejects the addition of symlinks.
    pub fn new() -> Self {
        RejectSymlinks
    }
}

impl ContentCheck for RejectSymlinks {
    fn name(&self) -> &str {
        "reject-symlinks"
    }

    fn check(&self, _: &CheckGitContext, content: &Content) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            match diff.status {
                StatusChange::Added |
                StatusChange::Modified(_) => (),
                _ => continue,
            }

            if diff.new_mode == "120000" {
                result.add_error(format!("{}adds a symlink at `{}` which is not allowed.",
                                         commit_prefix(content),
                                         diff.name));
            }
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::RejectSymlinks;
    use checks::test::*;

    const BAD_TOPIC: &str = "00ffdf352196c16a453970de022a8b4343610ccf";
    const FIX_TOPIC: &str = "d93ffc2e8b782ba8dce2278dd86fda0df80f454b";

    #[test]
    fn test_reject_symlinks() {
        let check = RejectSymlinks;
        let result = run_check("test_reject_symlinks", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit 00ffdf352196c16a453970de022a8b4343610ccf adds a symlink at `absolute-link` \
             which is not allowed.",
            "commit 00ffdf352196c16a453970de022a8b4343610ccf adds a symlink at `broken-link` \
             which is not allowed.",
            "commit 00ffdf352196c16a453970de022a8b4343610ccf adds a symlink at `inside-link` \
             which is not allowed.",
            "commit 00ffdf352196c16a453970de022a8b4343610ccf adds a symlink at `outside-link` \
             which is not allowed.",
        ]);
    }

    #[test]
    fn test_reject_symlinks_topic() {
        let check = RejectSymlinks;
        let result = run_topic_check("test_reject_symlinks_topic", BAD_TOPIC, check);
        test_result_errors(result, &[
            "adds a symlink at `absolute-link` which is not allowed.",
            "adds a symlink at `broken-link` which is not allowed.",
            "adds a symlink at `inside-link` which is not allowed.",
            "adds a symlink at `outside-link` which is not allowed.",
        ]);
    }

    #[test]
    fn test_reject_symlinks_topic_fixed() {
        let check = RejectSymlinks;
        run_topic_check_ok("test_reject_symlinks_topic_fixed", FIX_TOPIC, check);
    }
}
