// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

use std::char::REPLACEMENT_CHARACTER;

#[derive(Debug, Default, Clone, Copy)]
/// A check which denies commits which modify files containing special characters.
pub struct InvalidUtf8;

impl InvalidUtf8 {
    /// Create a check which rejects file contents which are invalid utf-8.
    ///
    /// Files may be marked as binary by unsetting the `text` attribute.
    pub fn new() -> Self {
        Self {}
    }
}

impl ContentCheck for InvalidUtf8 {
    fn name(&self) -> &str {
        "invalid-utf8"
    }

    fn check(&self, ctx: &CheckGitContext, content: &Content) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            match diff.status {
                StatusChange::Added |
                StatusChange::Modified(_) => (),
                _ => continue,
            }

            let diff_attr = ctx.check_attr("diff", diff.name.as_path())?;
            if let AttributeState::Unset = diff_attr {
                // Binary files should not be handled here.
                continue;
            }

            // FIXME: does this work for binary files?
            let patch = match content.path_diff(&diff.name) {
                Ok(s) => s,
                Err(err) => {
                    result.add_alert(format!("{}failed to get the diff for file `{}`: {}.",
                                             commit_prefix(content),
                                             diff.name,
                                             err),
                                     true);
                    continue;
                },
            };

            for line in patch.lines().filter(|line| line.starts_with('+')) {
                if line.contains(REPLACEMENT_CHARACTER) {
                    // Escape instances of backticks and backslashes.
                    let safe_line = line[1..]
                        .replace('\\', "\\\\")
                        .replace('`', "\\`");
                    result.add_error(format!("{}invalid utf-8 sequence added in \
                                              `{}`: `{}`.",
                                             commit_prefix_str(content, "not allowed;"),
                                             diff.name,
                                             safe_line));
                }
            }
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::InvalidUtf8;
    use checks::test::*;

    const BAD_TOPIC: &str = "cf16b71a21023320ffab7b3f7673dc62f33e5022";
    const FIX_TOPIC: &str = "e8763477e9ebef4a61d130724cee9e29b13f857e";

    #[test]
    fn test_invalid_utf8() {
        let check = InvalidUtf8;
        let result = run_check("test_invalid_utf8", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit cf16b71a21023320ffab7b3f7673dc62f33e5022 not allowed; invalid utf-8 sequence \
             added in `invalid-utf8`: `This file contains an invalid utf-8 sequence: \
             \u{fffd}`.",
        ]);
    }

    #[test]
    fn test_invalid_utf8_topic() {
        let check = InvalidUtf8;
        let result = run_topic_check("test_invalid_utf8_topic", BAD_TOPIC, check);
        test_result_errors(result, &[
            "invalid utf-8 sequence added in `invalid-utf8`: `This file contains an invalid utf-8 \
             sequence: \u{fffd}`.",
        ]);
    }

    #[test]
    fn test_invalid_utf8_topic_fixed() {
        let check = InvalidUtf8;
        run_topic_check_ok("test_invalid_utf8_topic_fixed", FIX_TOPIC, check);
    }
}
