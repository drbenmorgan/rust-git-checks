# v3.5.1

## Fixes

  * Now works with `arrayvec` (a dependency of `rayon-1.0.0`) which causes
    coersions to fail. See https://github.com/rust-lang/rust/issues/48406.

# v3.5.0

## Updated checks

  * `ValidName` may now be given domains to assume are valid.

# v3.4.0

## Updated checks

  * `ValidName` now caches its DNS query results for domain names. It is no
    longer `Copy`, but it is still `Clone` (the cache is cleared when cloned).
  * `Formatting` now cleans up child formatting processes which timed out.

# v3.3.0

  * The `Content` trait now can prepare a new workarea for a context using the
    `workarea` method.
  * `CheckGitContext` has deprecated `checkout` and `cd_to_worktree` methods
    as unsafe. They should have been `&mut self` methods.
  * `CheckGitContext` now has `workarea` and `workarea_mut` methods to get the
    underlying `GitWorkArea`.

# v3.2.1

## Updated checks

  * `ThirdParty` now refers to a "utility" rather than a "script" in its error
    messages.

# v3.2.0

  * Exposed a `FileName::new` method so that the filename parsing may be used
    externally.

## Updated checks

  * `Formatting` now supports naming the check differently than the attribute
    used to reformat. This may be used to refer to a formatter using a more
    specific name (such as with a version number).

## New checks

  * The `Changelog` checks that changes modify paths related to changelog
    entries. It supports the single file and directory of entries styles of
    changelog management.

# v3.1.1

  * Submodules which have different names and paths are now properly handled.

## Updated checks

  * `SubmoduleWatch` no longer messages about added modules if the module has
    been configured. This allows configuring the module to resolve the
    messages.

# v3.1.0

## New checks

  * The `LfsPointer` check verifies that paths with the `filter=lfs` attribute
    are valid according to the LFS specification.

# v3.0.0

  * Updated to `git-workarea-3.0.0`.

# v2.3.1

  * `Topic` has been fixed to give a diff against its merge base with the
    target branch rather than the current `HEAD` of the target branch.

# v2.3.0

  * `Commit::file_patch` may be used to get the patch contents of a path.
  * Added the `Topic` structure which represents an entire topic.
  * Both `Commit` and `Topic` implement the new `Content` check for use by
    checks which only care about content rather than Git-level metadata (such
    as topology and commit messages).
  * Added the `TopicCheck` trait for checking entire topics as a whole rather
    than individual commits.
  * Added the `ContentCheck` trait for checks which only care about `Content`.
  * The `impl_prelude` now contains two functions, `commit_prefix` and
    `commit_prefix_str` which create a `String` which mentions the commit for
    commits with an optional custom string.

## Updated checks

  * The following checks have been converted to `ContentChecks`:
    - `CheckEndOfLine`
    - `CheckExecutablePermissions`
    - `CheckSize`
    - `CheckWhitespace`
    - `Formatting`
    - `InvalidPaths`
    - `InvalidUtf8`
    - `RejectSymlinks`
    - `RestrictedPath`
  * `BadCommits` and `CheckExecutablePermissions` now support construction
    with iterators.

# v2.2.1

  * Private dependencies bumped.

# v2.2.0

  * Checks may now add a message with a configured severity level rather than
    calling different methods.

## Updated checks

  * `Formatting` now supports a timeout on formatting operations.

# v2.1.1

  * Dependency specifications for `git-workarea` and `error-chain` have been
    relaxed.

# v2.1.0

## Updated checks

  * `Formatting` now supports adding a message about how the formatting can be
    fixed.

# v2.0.0

  * Updated to `git-workarea-2.0.0`.
  * Checks may now mark that messages are temporary.
  * Checks must now be `Send` in order to support being run in parallel via
    `rayon`.
  * `CheckGitContext::cd_to_work_tree` may be used to run a command within the
    internal workarea.
  * `CheckGitContext::check_attr` now accepts arguments via `AsRef`.
  * `Commit::changed_files` may be used to get a list of changed files
    (symlinks and submodules are skipped).
  * `GitCheckConfiguration` now implements `Debug`.
  * There is now an `impl_prelude` module which contains all of the imports
    necessary to write checks.
  * `Commit::sha1_short` has been removed. Checks now always use the full
    commit hash in their messages.
  * Commit messages no longer have an extra newline at their end.

## Fixed checks

  * `CheckEndOfLine` no longer errors out when adding the missing newline at
    the end of a file.
  * `CheckSize` no longer tries to check submodule sizes (which are undefined
    in Git).
  * `SubmoduleRewind` now checks that a commit exists before checking for
    ancestry on a branch.
  * `ValidName` now collapses errors if the commiter and author are the same
    identity.
  * `ReleaseBranch` now checks that all merge bases are valid for the release
    branch.

## Updated checks

  * The `ValidName` check now supports different full name policies:
    - `Required` (the old behavior)
    - `Preferred` (just warns)
    - `Optional` (no diagnostics)
  * `RestrictedPath` can now warn about paths rather than only erroring.

## New checks

  * The `Formatting` check for checking formatting of files has been added.
    See the documentation for the requirements.

# v1.0.0

  * Initial stable release.
